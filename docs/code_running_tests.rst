**********************
Running the test suite
**********************

Preparation
===========

OpenREM is a Django application, and therefore we use Django's test-execution framework to test OpenREM.

The first thing to do is to install all of OpenREM's dependencies in a virtualenv. If you don't have this installed,
please see the links on the :doc:`install-prep` docs.

.. sourcecode:: bash

    mkdir veOpenREM
    virtualenv veOpenREM


next heading level
------------------

One more
^^^^^^^^
